import http from "../http-common";
class AboutDuaDataService {
    getAll() {
        return http.get("/aboutdua");
    }
    get(id) {
        return http.get(`/aboutdua/${id}`);
    }
    create(data) {
        return http.post("/aboutdua", data);
    }
    update(id, data) {
        return http.put(`/aboutdua/${id}`, data);
    }
    delete(id) {
        return http.delete(`/aboutdua/${id}`);
    }
}
export default new AboutDuaDataService();