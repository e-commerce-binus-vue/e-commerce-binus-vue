import http from "../http-common";
class AboutSatuDataService {
    getAll() {
        return http.get("/aboutsatu");
    }
    get(id) {
        return http.get(`/aboutsatu/${id}`);
    }
    create(data) {
        return http.post("/aboutsatu", data);
    }
    update(id, data) {
        return http.put(`/aboutsatu/${id}`, data);
    }
    delete(id) {
        return http.delete(`/aboutsatu/${id}`);
    }
}
export default new AboutSatuDataService();