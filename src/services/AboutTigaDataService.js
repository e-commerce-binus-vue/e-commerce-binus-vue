import http from "../http-common";
class AboutTigaDataService {
    getAll() {
        return http.get("/abouttiga");
    }
    get(id) {
        return http.get(`/abouttiga/${id}`);
    }
    create(data) {
        return http.post("/abouttiga", data);
    }
    update(id, data) {
        return http.put(`/abouttiga/${id}`, data);
    }
    delete(id) {
        return http.delete(`/abouttiga/${id}`);
    }
}
export default new AboutTigaDataService();