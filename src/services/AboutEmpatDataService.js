import http from "../http-common";
class AboutEmpatDataService {
    getAll() {
        return http.get("/aboutempat");
    }
    get(id) {
        return http.get(`/aboutempat/${id}`);
    }
    create(data) {
        return http.post("/aboutempat", data);
    }
    update(id, data) {
        return http.put(`/aboutempat/${id}`, data);
    }
    delete(id) {
        return http.delete(`/aboutempat/${id}`);
    }
}
export default new AboutEmpatDataService();