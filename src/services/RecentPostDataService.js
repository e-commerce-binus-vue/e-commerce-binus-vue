import http from "../http-common";
class RecentPostDataService {
    getAll() {
        return http.get("/recent");
    }
}
export default new RecentPostDataService();