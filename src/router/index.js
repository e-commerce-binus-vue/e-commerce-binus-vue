import Vue from "vue";
import Router from "vue-router";
import Home from "@/views/Home/Home.vue";
import Blogs from "@/views/Blogs/Blogs.vue";
import AboutUs from "@/views/AboutUs/AboutUs.vue";
import ContactUs from "@/views/ContactUs/ContactUs.vue";
import ProductDisplay from "@/views/ProductDisplay/productDisplay.vue";
import Admin from "@/views/Admin/Admin.vue";
import ProductAdmin from "@/views/pages/ProductAdmin.vue";
import ProfileAdmin from "@/views/pages/ProfilAdmin.vue";
import AboutAdmin from "@/views/pages/AboutAdmin.vue";
import ContactAdmin from "@/views/pages/ContactAdmin.vue";
import BlogAdmin from "@/views/pages/BlogAdmin.vue";

Vue.use(Router);

let router = new Router({
    routes: [{
            path: "/",
            name: "Home",
            components: {
                default: Home,
            },
        },
        {
            path: "/blog",
            name: "Blogs",
            components: {
                default: Blogs,
            },
        },
        {
            path: "/contact",
            name: "ContactUs",
            components: {
                default: ContactUs,
            },
        },
        {
            path: "/about",
            name: "AboutUs",
            components: {
                default: AboutUs,
            },
        },
        {
            path: "/products",
            name: "ProductDisplay",
            components: {
                default: ProductDisplay,
            },
        },
        {
            path: "/admin",
            name: "Admin",
            components: {
                default: Admin,
            },
        },
        {
            path: "/productAdmin",
            name: "ProductAdmin",
            components: {
                default: ProductAdmin,
            },
        },
        {
            path: "/profileAdmin",
            name: "ProfileAdmin",
            components: {
                default: ProfileAdmin,
            },
        },
        {
            path: "/aboutAdmin",
            name: "AboutAdmin",
            components: {
                default: AboutAdmin,
            },
        },
        {
            path: "/contactAdmin",
            name: "ContactAdmin",
            components: {
                default: ContactAdmin,
            },
        },
        {
            path: "/blogAdmin",
            name: "BlogAdmin",
            components: {
                default: BlogAdmin,
            },
        },
        {
            path: "/productAdmin/:id",
            name: "edit-product-admin",
            component: () =>
                import ("../views/pages/EditProductAdmin"),
        },
        {
            path: "/blogAdmin/:id",
            name: "edit-blog-admin",
            component: () =>
                import ("../views/pages/EditBlog"),
        },
        {
            path: "/aboutAdmin/aboutsatu=:id",
            name: "edit-about1-admin",
            component: () =>
                import ("../views/pages/EditAboutSatu"),
        },
        {
            path: "/aboutAdmin/aboutdua=:id",
            name: "edit-about2-admin",
            component: () =>
                import ("../views/pages/EditAboutDua"),
        },
        {
            path: "/aboutAdmin/abouttiga=:id",
            name: "edit-about3-admin",
            component: () =>
                import ("../views/pages/EditAboutTiga"),
        },
        {
            path: "/aboutAdmin/aboutempat=:id",
            name: "edit-about4-admin",
            component: () =>
                import ("../views/pages/EditAboutEmpat"),
        },
    ],
    mode: "history",
});

export default router;